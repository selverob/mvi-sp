\documentclass[12pt,a4paper,twocolumn]{article}
\usepackage[margin=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[section]{placeins}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\begin{document}
\title{NI-MVI \\ Milestone Report - ArXiv clustering}
\author{Róbert Selvek}
\maketitle

In this task, we are creating an efficient clustering model for papers from the
ArXiv e-print repository. This model should then be used to create a
recommendation engine. Apart from the actual model, a survey of the
state-of-the-art text clustering algorithms should be included in the final
report.

\section*{Literature survey}

Our literature survey began with the \emph{Sentence-BERT} paper
\cite{reimers2019sentencebert}, which was recommended in the task description.
Sentence-BERT is a modification of the original \emph{BERT} transformer network
\cite{devlin2019bert} and its variant with improved pre-training, \emph{RoBERTa}
\cite{liu2019roberta}. As demonstrated in \cite{reimers2019sentencebert}, the
main problem limiting the usefulness of BERT for clustering tasks is its
pairwise nature. The canonical way of evaluating sentence similarity with BERT
would entail submitting two sentences to a BERT model fine-tuned for the task.

The described approach is unviable for clustering over a dataset such as ArXiv
paper metadata. The Sentence-BERT paper reports time as long as 65 hours for
finding a pair of most similar sentences in a dataset with only 10,000
sentences. A possible improvement would be to directly derive sentence
embeddings from BERT's outputs and then apply another algorithm, such as
cosine-similarity over them. However, this approach doesn't perform well on
various sentence similarity tasks.

Sentence-BERT builds on BERT and allows generating semantically meaningful
sentence embeddings. It achieves this by using two BERT networks in a Siamese
Network architecture, which means that the networks have identical weights and
parameters. A pooling operation over the output of both BERT models is
performed. During training, a so-called \emph{triplet objective function} is
used, which uses three sentences - an anchor, a positive sentence and a negative
sentence. The positive sentence is similar to the anchor, while the negative one
is dissimilar. The objective function minimizes the distance between the anchor
and the positive sentence while maximizing the distance between the anchor and
the negative sentence.

Another widely used model for generating sentence embeddings which predates BERT
is the \emph{Universal Sentence Encoder} \cite{cer2018universal}. The paper
describes two variants of the model, both trained on data from Wikipedia, web
news, Q\&A pages, and other discussion forums. One of them is transformer-based.
It uses attention to take into account the context and position of words in a
sentence and then generate a vector encoding the sentence from the element-wise
sum of representations at each word position. Another variant is based on deep
averaging networks which, unusually for modern neural network-based approaches,
treat the input text as a bag of words (ignoring the word order)
\cite{DBLP:conf/acl/IyyerMBD15}. While this reduces the quality of the results,
these networks have lower resource consumption and are faster to train.

An interesting twist on document embeddings generation with a large relevance to
my work is SPECTER \cite{cohan2020specter}. SPECTER is a method for generating
document-level embeddings based on SciBERT \cite{DBLP:conf/emnlp/BeltagyLC19}, a
variant of BERT trained on a corpus of 1.14 million scientific papers from
the fields of computer science and biomedicine.

The key innovation in SPECTER is its use of scientific paper citations as a signal
of an existing relation between documents. During training, a triplet loss
function similar to Sentence-BERT is used. The positive sentence is a paper that
is cited by the anchor paper (called "query paper" by the authors of SPECTER)
and the negative sentence is a paper that is not cited by the anchor paper. To
improve training, the negative sentences are not only chosen randomly from the
corpus but they are complemented by the so-called "hard negatives" - papers not
cited by the anchor paper but cited by a paper that is cited by the anchor.

\section*{Current work}

The input data for this task consists of a JSON file containing metadata of
1980927 papers from ArXiv. From each paper's metadata, I generate a sentence,
which a model will use to compute an embedding corresponding to the paper. The
sentence consists of the concatenation of the title and the abstract of the
paper, cleaned by removing newlines and LaTeX/MathJaX control characters. 
To fulfill both parts of the task assignment (both making a clustering model and
a recommendation engine), I process the documents in two phases. 

In the first phase, I generate embeddings for all documents using
\texttt{sentence-transformers} library written by the authors of Sentence-BERT.
I use this library with the SPECTER model described in the previous section. The
generated embeddings enable the first use-case - recommending similar papers.
For a given set of ArXiV paper IDs, the user can perform a cosine-similarity
search between the embeddings of the documents generated for given papers and
embeddings of all other papers on ArXiv.

For the second part of the assignment, clustering, I use the BERTopic algorithm
\cite{grootendorst2020bertopic}. This algorithm finds dense clusters in the
space of document embeddings and detects the most important words for papers in
each cluster, thus generating interpretable topics.

While I am able to incrementally generate embeddings for all papers, it is
computationally infeasible for me to run the clustering algorithm over the whole
dataset. Thus I use a random sample of 10\% of the papers to generate the
topics. The trained clustering model then predicts the topics of the remaining
90\% of papers by finding the cluster in the vector space into which their
embeddings fall. To reduce the number of topics, we use BERTopic automatic
reduction functionality which merges topics which are more than 90\% similar,
i.e. 90\% of their defining words are the same.

\section*{Results}

\subsection*{Recommendation}

For the recommendation task, I prepared a class which, given embeddings for
papers and IDs of the papers for which the user wants recommendations to be
generated, returns the most similar papers, measured by cosine similarity of
their embeddings.

To evaluate the quality of recommendations, I used ArXiv categories, which are
assigned to papers by their authors. For each paper from a sample of 19809, I
generated 5 recommendations and measured how many of the recommendations belong
to the same \emph{archive}, which is the term ArXiv uses to describe a top level
category.

The mean number of recommended papers from matching archives was $2.57$ out of 5
recommendations. While this may seem like a bad result, it is partially caused
by the fact that ArXiv's category hierarchy is not well-designed, \footnote{To
give an example, there is an archive for papers related to physics, which
includes accelerator physics. However, high energy physics papers are divided
into 4 separate archives. Thus, a paper about particle accelerators may end up
categorized under the physics archive or under high energy physics experiments
archive.}. Partially, the given result is desirable, since if the recommendation
article only returned articles from the same category, the user wouldn't be able
to discover new papers.

\subsection*{Clustering}

The BERTopic algorithm clustered the papers into 338 topics. Their intertopic
distances, as calculated by the LDAVis algorithm \cite{ldavis} can be seen on
figure~\ref{fig:ldavis}. 

Unfortunately, the generated clusters seem to have a very low quality. Out of
198092 training documents, 37958 have been discarded as outliers and 148213
documents have been merged into a single cluster characterized by non-specific
words such as \emph{where}, \emph{when}, \emph{properties} and \emph{problem},
leaving only 11921 documents (6\%) in clusters which are interpretable.

Solving this problem will be my major focus in the remaining work on this task.

\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{topicplot.png}
    \caption{LDAvis visualization of inter-topic distances}
    \label{fig:ldavis}
\end{figure}

\renewcommand\refname{References}

\bibliography{report}
\bibliographystyle{halpha}


\end{document}
