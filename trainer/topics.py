import pickle
import gzip
import pandas as pd
import json
import random
from tqdm.autonotebook import tqdm
from math import floor
import pickle
from bertopic import BERTopic

class PaperRepository:
  """
  Enables parsing of ArXiv metadata exports and efficient extraction of some
  paper features and "documents" - strings with paper title, authors and 
  abstract which can be processed by sentence-BERT based algorithms
  """
  def __init__(self, path_to_file, random_seed=42, separator="\n"):
    self.id_to_document = {}
    self.id_to_categories = {}
    self.category_ids = {}
    self.category_names = []
    self.category_to_ids = []
    self.id_to_authors = {}
    self.random = random.Random(random_seed)
    with open(path_to_file, "rb") as f:
      for line in tqdm(f, total=1978109):
        self.__process_json_line(line, separator)
  
  def document_for_id(self, doc_id):
    return self.id_to_document[doc_id]
  
  def documents_for_ids(self, doc_ids):
    return [self.id_to_document[doc_id] for doc_id in doc_ids]
  
  def categories_for_id(self, doc_id):
    return [self.category_names[cat] for cat in self.id_to_categories[doc_id]]

  def ids_for_category(self, category):
    return self.category_to_ids[self.__category_id(category)]
  
  def authors_for_id(self, doc_id):
    raise NotImplementedError("Disabled due to memory usage issues")
    return self.id_to_authors[doc_id]

  def sample_category(self, category, sample_size):
    return self.random.sample(self.ids_for_category(category), sample_size)

  def sample_numbers(self, ratio):
    total_docs = len(self.id_to_document)
    return self.random.sample(range(0, total_docs), floor(total_docs * ratio))
  
  def sample_all_categories(self, ratio):
    sampled_ids = set()
    for category in self.category_ids.keys():
      sample_size = floor(len(self.ids_for_category(category)) * ratio)
      if sample_size < 25:
        sampled_ids.update(self.ids_for_category(category))
      else:
        sampled_ids.update(self.sample_category(category, sample_size))
    return list(sampled_ids)
  
  def sample_all_simple(self, ratio):
    total_docs = len(self.id_to_document)
    return self.random.sample(list(self.id_to_document.keys()), floor(total_docs * ratio))

  def __category_id(self, category):
    if category in self.category_ids:
      return self.category_ids[category]
    else:
      cat_id = len(self.category_ids)
      self.category_ids[category] = cat_id
      self.category_to_ids.append([])
      self.category_names.append(category)
      return cat_id

  def __process_json_line(self, line, separator):
    paper_dict = self.__make_paper_dict(line)
    pid = paper_dict["id"]
    self.id_to_document[pid] = self.__paper_dict_to_document(paper_dict, separator)
    self.id_to_categories[pid] = [self.__category_id(cat) for cat in paper_dict["categories"]]
    #self.id_to_authors[pid] = paper_dict["authors"]
    for category in paper_dict["categories"]:
      self.category_to_ids[self.__category_id(category)].append(pid)

  def __make_paper_dict(self, file_line):
    object = json.loads(file_line)
    title = self.__clean_text(object["title"])
    abstract = self.__clean_text(object["abstract"])
    return {
        "id": object["id"],
        "title": title,
        "abstract": abstract,
        "categories": object["categories"].split(" "),
        "authors": [", ".join(filter(lambda s: s != "", a)) for a in object["authors_parsed"]]
    }
  
  def __clean_text(self, text):
    return text.replace("\n", " ").replace("$", "").replace("\\", "")

  def __paper_dict_to_document(self, paper_dict, separator):
    # Authors could be a useful signal, but there are so many "generic" Asian
    # names in the papers, that they end up forming a cluster by themselves
    # return f"{paper_dict['title']}\n{'; '.join(paper_dict['authors'])}\n{paper_dict['abstract']}"
    return f"{paper_dict['title']}{separator}{paper_dict['abstract']}"


if __name__ == "__main__":
  loaded = {}

  print("Loading embeddings")
  with open("/home/selverob/work/spectre_100pct_embeddings.pkl", "rb") as fin:
      loaded = pickle.load(fin)
  print("Loaded embeddings")

  pr = PaperRepository("/home/selverob/work/arxiv-metadata-oai-snapshot.json", separator="[SEP]")
  
  train_ids = loaded["ids"][::2]
  train_embeddings = loaded["embeddings"][::2]

  topic_model = BERTopic(embedding_model="allenai-specter", nr_topics="auto")
  topics = topic_model.fit_transform(pr.documents_for_ids(train_ids), embeddings=train_embeddings)
  topic_model.save("/content/drive/MyDrive/Colab Notebooks/bertopic_spectre_50pct")
  with open("/home/selverob/work/bertopic_spectre_50pct_topics", "wb") as out:
      pickle.dump(topics, out)
