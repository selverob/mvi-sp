\documentclass[12pt,a4paper,twocolumn]{article}
\usepackage[margin=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[section]{placeins}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{subcaption}
\begin{document}
\title{NI-MVI \\ Project Report - ArXiv clustering}
\author{Róbert Selvek}
\maketitle

In this task, I am creating an efficient embedding model for papers from the
ArXiv e-print repository. I then use this model to implement a paper
recommendation engine and to cluster the papers. I include a survey of
state-of-art textual embedding and clustering algorithms in this report.

\section{Literature survey}

I began the literature survey with the \emph{Sentence-BERT} paper
\cite{reimers2019sentencebert}, recommended in the task description.
Sentence-BERT is a modification of the original \emph{BERT} transformer network
\cite{devlin2019bert} and its variant with improved pre-training, \emph{RoBERTa}
\cite{liu2019roberta}. As demonstrated in \cite{reimers2019sentencebert}, the
main problem limiting the usefulness of BERT for clustering tasks is its
pairwise nature. The canonical way of evaluating sentence similarity with BERT
would entail submitting two sentences to a BERT model fine-tuned for the task.

The described approach is unviable for clustering over a dataset such as ArXiv
paper metadata. The Sentence-BERT paper reports that finding a pair of most
similar sentences in a dataset with only 10,000 sentences can take as long as 65
hours. A possible improvement would be to directly derive sentence embeddings
from BERT's outputs and then apply another algorithm, such as cosine-similarity
over them. However, this approach doesn't perform well on various sentence
similarity tasks.

Sentence-BERT builds on BERT and allows generating semantically meaningful
sentence embeddings. It achieves this by using two BERT networks in a
\emph{Siamese Network} architecture, which means that the networks have
identical weights and parameters. Siamese networks perform a pooling operation
over the output of both BERT models. The training process uses a \emph{triplet
objective function} with three input sentences - an anchor, a positive
sentence, and a negative sentence. The positive sentence is similar to the
anchor, while the negative one is dissimilar. The objective function minimizes
the distance between the anchor and the positive sentence while maximizing the
distance between the anchor and the negative sentence.

A widely used sentence embedding model predating BERT is the \emph{Universal
Sentence Encoder} \cite{cer2018universal}. The paper describes two variants of
the model, both trained on data from Wikipedia, web news, Q\&A pages, and other
discussion forums. One of them is transformer-based. It uses attention to take
into account the context and position of words in a sentence and then generate a
vector encoding the sentence from the element-wise sum of representations at
each word position. Another variant is based on deep averaging networks which,
unusually for modern neural network-based approaches, treat the input text as a
bag of words (ignoring the word order) \cite{DBLP:conf/acl/IyyerMBD15}. While
this reduces the quality of the results, these networks have lower resource
consumption and are faster to train.

An interesting twist on document embeddings generation with a large relevance to
my work is SPECTER \cite{cohan2020specter}. SPECTER is a method for generating
document-level embeddings based on SciBERT \cite{DBLP:conf/emnlp/BeltagyLC19}, a
variant of BERT trained on a corpus of 1.14 million scientific papers from
the fields of computer science and biomedicine.

The key innovation in SPECTER is its use of scientific paper citations as a
signal of existing relations between documents. During training, it uses a
triplet loss function similar to Sentence-BERT. The positive sentence is a paper
cited by the anchor paper (called "query paper" by the authors of SPECTER) and
the negative sentence is a paper not cited by the anchor paper. To improve
training, randomly chosen negative sentences are complemented by the so-called
\emph{hard negatives} - papers not cited by the anchor paper but cited by a paper
that is cited by the anchor.

\section{Methods}

The input data for this task consists of a JSON file containing metadata of
1980927 papers from ArXiv. For each paper, I generate a sentence, which my model
will use to compute an embedding corresponding to the paper. Each sentence
consists of the concatenation of the paper's title and abstract. These are
cleaned by removing newlines and LaTeX/MathJaX control characters. To
fulfill both parts of the task assignment (both making a clustering model and a
recommendation engine), I process the documents in two phases. 

In the first phase, I generate embeddings for all documents using
\texttt{sentence-transformers} library written by the authors of Sentence-BERT.
I use this library with the SPECTER model described in the previous section. The
generated embeddings enable the first use-case - recommending similar papers.
For a given set of ArXiV paper IDs, the user can perform a cosine-similarity
search between the embeddings of the documents generated for given papers and
embeddings of all other papers on ArXiv.

For the second part of the assignment, clustering, I use the BERTopic algorithm
\cite{grootendorst2020bertopic}. This algorithm finds dense clusters in the
space of document embeddings and detects the most important words for papers in
each cluster, thus generating interpretable topics.

To find interpretable clusters among the embeddings, BERTopic performs three
steps. First, the dimensionality of the embeddings is reduced using the
\emph{UMAP} algorithm \cite{mcinnes2018umap-software}. Then, an \emph{HDBSCAN}
model \cite{mcinnes2017hdbscan} is used to search for clusters in the
low-dimensional space. Once the clusters are identified, c-TF-IDF identifies
the most relevant words to describe each cluster.

In contrast with SPECTER, which doesn't require any fine-tuning, BERTopic
requires care and a long process of experimentation to find suitable parameters
for its submodels and itself. I have obtained the best results with the
following set-up:

\begin{itemize}
    \item \emph{UMAP} reduces the dimensionality of embeddings to 3 dimensions.
    Minimum distance of points in the reduced-dimension space is 0 and 30
    neighboring points are taken into account when reducing the dimensionality.
    The default parameters of 5 dimensions and 15 neighbors led to loss of local
    structure during dimensionality reduction and points in the 5-dimensional
    space were too distant for the HDBSCAN algorithm to identify a meaningful
    number of clusters in them.
    \item \emph{HDBSCAN} generates clusters with a minimum size of 30. Since the
    default settings disregard over 20\% of the training set as noise, I have
    reduced the \emph{min\_samples} parameter to 2.
    \item To calculate the most relevant terms in each topic, c-TF-IDF uses a
    \emph{CountVectorizer} from scikit-learn. My customized vectorizer counts the
    occurences of single words and bigrams and disregrards stop words from
    scikit's built-in English list.
\end{itemize}

While it is possible and necessary to generate embeddings for all papers, it is
computationally infeasible for me to run the clustering algorithm over the whole
dataset, due to the large memory requirements of UMAP and HDBSCAN. Thus I use a
training set of 10\% of the papers to generate the topics. The trained
clustering model then predicts the topics of the remaining 90\% of papers by
finding the cluster in the vector space into which their embeddings fall.

\begin{figure*}[t!]
    \centering
    \begin{tabular}{ | l || c | c | c | c |}
      \hline
      Metric & Accuracy & Precision & Recall & F1 Score \\
      \hline
      Cosine similarity & 0.996 & 0.996 & 0.999 & 0.998  \\
      Manhattan distance & 0.996 & 0.996 & 0.999 & 0.998  \\
      Euclidean distance & 0.996 & 0.996 & 0.999 & 0.998  \\
      Dot product & 0.996 & 0.996 & 0.999 & 0.998  \\
      \hline
    \end{tabular}
    \caption{\emph{BinaryClassificationEvaluator} results for SPECTER embeddings}
    \label{table:bec}
\end{figure*}  


\section{Results}

\subsection{Embedding quality}

To evaluate the quality of the generated embeddings, I use the
\emph{BinaryClassificationEvaluator} from sentence-transformers library. This
evaluator considers the distance between embeddings generated for different
sentences as the output of a binary classifier which distinguishes similar and
disimilar sentences. It then calculates the accuracy, precision, recall and F1
score.

I have generated 3 000 000 pairs of papers. Papers were considered to be similar
if the Jaccard similarity of the sets of categories assigned by their submitters
was at least 0.7 and disimilar when it was at most 0.3.

Table~\ref{table:bec} shows the results of the evaluation. While these results
may seem implausibly good, it bears emphasising that the SPECTER model is
specifically trained on computer science and medicine papers. Since ArXiv is
primarily a repository for computer science papers, it is highly probable that
some of the papers have actually been in the training set of the model.

\subsection{Recommendation}

\begin{listing*}
    \begin{minted}{Python}
recommender = Recommender(paper_repository)

# Running the recommender on the paper describing the SPECTER embedding model
similar_papers = recommender.get_similar_papers(["2004.07180"])[0]
print(similar_papers)
# Output: 
# ['2004.06180', '2004.08180', '2004.09180', '2004.03180', '2004.05180']

for document in paper_repository.documents_for_ids(similar_papers):
    # Separating the title from the abstract
    print(document.split("[SEP]")[0])

# Output:
# Challenges and Opportunities for Computer Vision in Real-life Soccer Analytics
# A Hierarchical Convex Optimization for Multiclass SVM
#   (cont.)  Achieving Maximum Pairwise Margins with Least Empirical Hinge-Loss
# How Value-Sensitive Design Can Empower Sustainable Consumption
# Towards Multimodal Simultaneous Neural Machine Translation
# The Parallax of VHS J1256-1257 from CFHT and Pan-STARRS 1

    \end{minted}
    \caption{A sample interaction with the recommendation library}
    \label{lst:recommend}
\end{listing*}

For the recommendation task, I prepared a class which returns papers most
similar to papers from the given set, measured by cosine similarity of their
embeddings.

To evaluate the quality of recommendations, I used ArXiv categories, which are
assigned to papers by their authors. For each paper from a sample of 19809, I
generated 5 recommendations and measured how many of the recommendations belong
to the same \emph{archive}, which is the term ArXiv uses to describe a top level
category.

The mean number of recommended papers from matching archives was $2.57$ out of 5
recommendations. While this may seem like a bad result, it is partially caused
by the fact that ArXiv's category hierarchy is not well-designed, \footnote{To
give an example, there is an archive for papers related to physics, which
includes accelerator physics. However, high energy physics papers are divided
into 4 separate archives. Thus, a paper about particle accelerators may end up
categorized under the physics archive or under high energy physics experiments
archive.}. Partially, the given result is desirable, since if the recommendation
article only returned articles from the same category, the user wouldn't be able
to discover new papers.

In listing~\ref{lst:recommend}, I show a sample interaction with the
recommendation class mentioned at the beginning of this section. The user asks
for 5 recommendations of papers similar to the SPECTER paper, which describes
the embedding model used in this project. The recommender returns 5 papers with
the closest embeddings to the query paper, as measured by cosine similarity.

4 out of 5 recommended papers are related to data science. One of them is
specifically about natural language processing, one about applied computer
vision, one about a specific implementation of the SVM algorithm. Moreover,
while it may seem unrelated, the paper titled "How Value-Sensitive Design Can
Empower Sustainable Consumption" deals with designing a shopping assistant
application which would help consumers buy more sustainable goods in grocery
stores and about how machine learning and artificial technologies could be
leveraged to that goal.

Although one of the papers is wholly unrelated to the subject matter of the
query paper, the remaining ones are at least partially related, even if not to
the specific topic of language embedding models. Thus a possible use-case of my
algorithm could be in the initial phase of exploring a new field, where the user
wants to be exposed to multiple subfields.

\subsection{Clustering}

\begin{figure*}[t]
    \centering
    \includegraphics[width=0.7\linewidth]{reduced.png}
    \caption{Reduced dimensionality embedding of training documents, colored according to topic assigned by BERTopic. Please see notes in the related chapter 3.3 for access to better visualizations.}
    \label{fig:umap}
\end{figure*}

During initial experiments, the BERTopic algorithm clustered the training papers
into 1800 topics. To increase topic sizes, I set the maximum number of topics to
500. 76204 training documents (38\%) were classified as noise and not assigned a
topic. Figure~\ref{fig:umap} shows a plot of reduced-dimensionality embeddings
of the training documents colored according to their assigned topic. 

However, the plot referenced above may be very confusing, since it only uses 24
colors to color 500 different topics. As such, it looks like there are only few
topics which are spread out in the embedding space. For an equivalent
interactive visualization in which you can explore the embedding space and
topics assigned to the training papers, please refer to the
\texttt{visualizations/embeddings.html} file available in the repository. In
this interactive visualization, you can filter papers by topics, see the 3 most
representative words for each topic and the names of the papers assigned to the
topic.

In the same vein, to allow exploration of the topic space without the clutter
associated with showing each training paper, I prepared another visualization
available in \texttt{visualizations/topics.html} file in the repository. Each
point on this visualization corresponds to the central point of its respective
topic - the mean of reduced-dimensionality embeddings of training papers
classified into the topic. The sizes of points in the visualization correspond
to the sizes of the topics.

After predicting topics for all ArXiv papers, 827833 (41\%) papers were
classified as noise. The largest topic contained 17847 papers, and was
characterized by the following keywords: "quark, neutrino, higgs, qcd, decays."
This corresponds well to the categories of the papers from the topic. Papers in
this topic were categorized primarily under the following 5 categories: High
Energy Physics Phenomenology, Theory, Experiments, Nuclear Theory, and
Astrophysics.


\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{topic_sizes.pdf}
    \caption{Histogram of topic sizes}
    \label{fig:size_histo}
\end{figure}

The smallest topic contained 546 papers, primarily from the field of
mathematical physics and semiconducting nanostructures. Mean topic size was 2306
papers, the median topic contained 1586 papers. A histogram of topic sizes is
shown on figure~\ref{fig:size_histo} 

Since the only human-assigned labels available for each paper are its
categories, I also calculated some metrics about the relation between topics and
categories. The topic with the largest number of unique categories assigned to
its papers contains papers from 156 categories out of 176 categories available
on ArXiv. The topic with the smallest such number contains papers from 12
categories. Mean number of categories among all topics was 118. The median topic
contains papers from 138 categories. However, this can be expected, since the
mean number of categories assigned to a paper is 1.66 and 308526 (15\%) of
papers have at least 3 categories.

To gain more information from the noisy category data, I used a metric called
\emph{category cohesion}. Category cohesion of a topic is the ratio of papers
which are assigned at least one of the most common 10\% of categories which
appear in the topic. Among the topics generated by the model, the mean category
cohesion is 0.58. The median topic has a category cohesion of 0.51. A histogram
of category cohesions is shown on figure~\ref{fig:cohesion_histo}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{cohesion_ratios.pdf}
    \caption{Histogram of category cohesions}
    \label{fig:cohesion_histo}
\end{figure}

\section{Further Work}

Since both the embeddings and topic predictions can be generated incrementally,
as papers are being added to the dataset, there is little standing in the way of
deploying this model and allowing users to use its recommendations. However the
topic generation and clustering seems to be relatively poor. Thus, more
experiments in these directions could be performed:

\begin{itemize}
    \item \emph{Increasing the size of the training set} - while 10\% of all
    papers may seem like a large training set, there are many niche topics in
    computer sciense and physics with only single-digit number of papers
    published. There is only a small chance multiple papers from such topic
    would be included in a 10\% traning set, so the topic gets completely
    missed. An increase in the training set size would go hand in hand with
    lowering the minimum topic size.
    \item \emph{Further experimentation with dimensionality reduction} -
    BERTopic uses a default of 5 dimensions during dimensionality reduction. To
    improve the visualizability of embeddings and due to the poor results
    achieved with this default, I reduced the embeddings to 3 dimensions.
    However, HDBSCAN performs well on data with up to 50 dimensions. With a
    larger training set and more computational power, it should be possible to
    preserve more data from the raw embeddings.
    \item \emph{Better text pre-processing} - When importing the dataset, I
    strip all LaTeX control sequences from the text. However, abstracts often
    include mathematical formulas which are a major signal about the content of
    the paper. Using a library such as Pandoc \cite{pandoc}, it should be
    possible to convert them to plain text and pass them to the embedding model.
    When beginning to work on the project, I tried to use this approach, however
    simply calling Pandoc proved to be too slow to process all documents within
    a reasonable timespan.
\end{itemize}

\renewcommand\refname{References}

\bibliography{report}
\bibliographystyle{halpha}


\end{document}
