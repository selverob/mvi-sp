NI-MVI ArXiv recommender
==========

Assignment
----
For the dataset containing metadata from the ArXiv webpage
https://www.kaggle.com/Cornell-University/arxiv create an efficient clustering
model. Make a review of current state-of-the-art clustering algorithms for text
and choose an appropriate one (maybe sentence-BERT?)

Data download
-----
Current datasets are always available in the Kaggle assignment

Running the code
-----

The code is contained in the `project.ipynb` file in this repository. I can
provide the computed embeddings and the BERTopic model upon request. However,
since in total their files are 9 gigabytes large I'm not uploading them to the
repository.
