% Inbuilt themes in beamer
\documentclass{beamer}

\usepackage{verbatim}
\usepackage{minted}

% Theme choice:
\usetheme{Madrid}

% Title page details: 
\title{ArXiv Clustering And Recommendation} 
\author{Róbert Selvek}
\date{\today}


\begin{document}

% Title page frame
\begin{frame}
    \titlepage 
\end{frame}

% Remove logo from the next slides
\logo{}


% Outline frame
\begin{frame}{Outline}
    \tableofcontents
\end{frame}


% Lists frame
\section{Goals}
\begin{frame}{Goals}

\begin{itemize}
    \item We have a \href{https://www.kaggle.com/Cornell-University/arxiv}{dataset} of abstracts and metadata of all papers on ArXiv. What can we do with it?
    \item Let's try to apply the magic of modern neural networks to them and use it to
    \begin{itemize}
        \item Build a simple recommender
        \item See if we can cluster the papers into topics
    \end{itemize}
\end{itemize}
\end{frame}

\section{Methods}
\begin{frame}[fragile]{Data}
    \begin{itemize}
        \item 3.1GB JSON file from Kaggle
        \item For each paper we have its title, abstract, authors and submitter,
        categories and some other data
        \item Title and abstract are pure \LaTeX, they need to be cleaned
        \begin{itemize}
            \item Re-rendering as plain-text infeasible, we're limited to simple text replace
        \end{itemize}
    \end{itemize}

    \begin{block}{This is not great for NLP!}
        \begin{verbatim}
A study of $B_{d}^0 \to J/\Psi \eta^{(\prime)}$ Decays [...]
        \end{verbatim}
    \end{block}
\end{frame}

\begin{frame}{Outline of The Process}
    \begin{enumerate}
        \item Pick a model to generate embeddings for the papers
        \item Generate embeddings for all papers
        \item Run a clustering algorithm on a training set
        \item Allocate the non-training papers to the found clusters
    \end{enumerate}
\end{frame}

\begin{frame}{The Embedding Model}
    \begin{columns}[onlytextwidth,T]
        \column{\dimexpr\linewidth-50mm-5mm}
        \begin{itemize}
            \item \textbf{SPECTER}
            \item A (Sci)BERT-based model which uses paper citations to inform
            embeddings
            \item Trained on papers from medicine and computer science. Let's
            see how it does on physics papers.
            \item We create documents for SPECTER by concatenating the cleaned
            title and abstract of the paper
            \item Recommendation is just a cosine-similarity search away
            \item Generating the embeddings was a computational challenge
        \end{itemize}
        \column{50mm}
        \begin{center}
            \includegraphics[width=50mm]{specter.jpg}
        \end{center}
    \end{columns}
\end{frame}

\begin{frame}
    \includegraphics[width=\linewidth]{checkpoint.jpg}
\end{frame}

\begin{frame}{Clustering}
    \begin{itemize}
        \item 10\% training set (~200 000 papers)
        \item Three basic steps:
        \begin{enumerate}
            \item Reduce dimensionality
            \begin{itemize}
                \item UMAP algorithm
                \item 768 dimensions $\rightarrow$ 3 dimensions
                \item No minimum separation between points, 30 neighbors taken into account
            \end{itemize}
            \item Find dense clusters
            \begin{itemize}
                \item HDBSCAN
                \item Identifies dense clusters, discards outliers
                \item Minimum cluster size is 30, lowered the required density, 500 topics generated
            \end{itemize}
            \item Create topic representation
            \begin{itemize}
                \item c-TF-IDF
                \item Finds the representative words (bigrams) for each topic
            \end{itemize}
        \end{enumerate}
        \item Ambivalent about BERTopic
    \end{itemize}
\end{frame}

\section{Results}

\begin{frame}{Embedding generation}
    \begin{table}[t!]
        \centering
        \begin{tabular}{ | l || c | c | c | c |}
          \hline
          Metric & Accuracy & Precision & Recall & F1 Score \\
          \hline
          Cosine similarity & 0.996 & 0.996 & 0.999 & 0.998  \\
          Manhattan distance & 0.996 & 0.996 & 0.999 & 0.998  \\
          Euclidean distance & 0.996 & 0.996 & 0.999 & 0.998  \\
          Dot product & 0.996 & 0.996 & 0.999 & 0.998  \\
          \hline
        \end{tabular}
    \end{table}  
    \begin{itemize}
        \item Running \emph{BinaryClassificationEvaluator} based on 3 000 000
        pairs of papers classified as similar or disimilar based on Jaccard
        similarity of their category sets
        \item Very suspicious results.
        \begin{itemize}
            \item Misconfigured tresholds?
            \item Evaluating on the underlying training set?
            \item Misusing the evaluator?
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Recommendation}
    \begin{itemize}
        \item 1\% evaluation set
        \item Mean $2.57$ recommended papers out of 5 belong to the same archive
        \item Class for recommentations
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \begin{minted}[fontsize=\footnotesize]{Python}
recommender = Recommender(paper_repository)

# Running the recommender on the paper describing SPECTER
similar_papers = recommender.get_similar_papers(["2004.07180"])[0]
print(similar_papers)
# Output: 
# ['2004.06180', '2004.08180', '2004.09180', '2004.03180', '2004.05180']

for document in paper_repository.documents_for_ids(similar_papers):
    # Separating the title from the abstract
    print(document.split("[SEP]")[0])

# Output:
# Challenges and Opportunities for Computer Vision in Real-life 
#   (cont.) Soccer Analytics
# A Hierarchical Convex Optimization for Multiclass SVM
#   (cont.)  Achieving Maximum Pairwise Margins with Least 
#   (cont.) Empirical Hinge-Loss
# How Value-Sensitive Design Can Empower Sustainable Consumption
# Towards Multimodal Simultaneous Neural Machine Translation
# The Parallax of VHS J1256-1257 from CFHT and Pan-STARRS 1
    
    \end{minted}
\end{frame}

\begin{frame}{Clustering - Topic sizes}
    \centering
    \includegraphics[width=0.8\linewidth]{topic_sizes.pdf}

    Smallest topic has 546 papers, mean 2306, largest 17848

    41\% of papers classified as outliers
\end{frame}

\begin{frame}{Clustering - Topic cohesion}
    \centering
    \includegraphics[width=0.8\linewidth]{cohesion_ratios.pdf}

    How many papers from a topic belong to 10\% of its most common categories?

    Mean cohesion is 58\%.
\end{frame}

\begin{frame}{Final Words}
    \begin{itemize}
        \item Assignment essentially fulfilled:
        \begin{itemize}
            \item Recommendations worked
            \item Clustering performed
            \item Metrics \emph{seem} good
        \end{itemize}
        \item \textbf{However:}
        \begin{itemize}
            \item I don't understand large parts of the dataset
            \item The recommendations just don't look useful
            \item Possibly using another embedding model could have been better
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Thank you for the attention!}
    \begin{center}
        \includegraphics[width=\linewidth]{work.jpg}
    \end{center}
\end{frame}
\end{document}
